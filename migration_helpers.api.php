<?php

/**
 * @file
 * Examples of how to use migration_helpers functions.
 */

/**
 * Rename and move select terms to new vocabulary and term reference field.
 *
 * Here we move select terms from the deprecated 'Design sector' vocabulary
 * (note the source vocabulary does not get specified, as we are only moving
 * terms that are referenced by the chosen entity type (defaults to node) and
 * bundle) from the design_type field into the new Focus Areas vocabulary which
 * is referenced by `field_focus`.  We rename three of the terms and explicitly
 * discard four others.  Any we do not put in the mapping will be brought in
 * with their current name.  This does not currently copy over term description
 * or anything else, only term label.  Perhaps it could be reworked to be both
 * more generic and more powerful with an entity clone.
 */
function example_deploy_design_sector_to_focus_area() {
  $mapping = [
    'Memorial' => 'Public Memory',
    'Housing' => 'Social Cohesion',
    'Health' => 'Health Equity',
    'Education' => FALSE,
    'Equity' => FALSE,
    'Office Space' => FALSE,
    'Other' => FALSE,
  ];
  migration_helpers_move_terms(
    destination_vocabulary: 'focus_area',
    source_field: 'field_design_type',
    destination_field: 'field_focus',
    bundle: 'work',
    mapping: $mapping,
  );
}
