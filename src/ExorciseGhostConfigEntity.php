<?php

namespace Drupal\migration_helpers;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;

/**
 *
 *
 * Command to fix "non-existent config entity name returned by FieldStorageConfigInterface::getBundles()" errors; see https://www.drupal.org/project/drupal/issues/2916266
 */
class ExorciseGhostConfigEntity {

  /**
   * The key value store to use.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValueStore;

  /**
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   The key value store to use.
   */
  public function __construct(KeyValueFactoryInterface $key_value_factory) {
    $this->keyValueStore = $key_value_factory;
  }

  /**
   * Corrects a field storage configuration. See https://www.drupal.org/project/drupal/issues/2916266 for more info
   *
   * @command update:correct-field-config-storage
   *
   * @param string $entity_type
   *   Entity type
   * @param string $bundle
   *   Bundle name
   * @param string $field_name
   *   Field name
   */
  public function removeFieldStorageConfig($entity_type, $bundle, $field_name) {
    $field_map_kv_store = $this->keyValueStore->get('entity.definitions.bundle_field_map');
    $map = $field_map_kv_store->get($entity_type);
    unset($map[$field_name]['bundles'][$bundle]);
    $field_map_kv_store->set($entity_type, $map);
  }

  /**
   * Remove an entire bundle.
   */
  public function removeBundleFromFieldStorageConfig($entity_type, $bundle) {
    $field_map_kv_store = $this->keyValueStore->get('entity.definitions.bundle_field_map');
    $map = $field_map_kv_store->get($entity_type);
    foreach ($map as $field_name => $info) {
      unset($map[$field_name]['bundles'][$bundle]);
      if (!isset($map[$field_name]['bundles']) || empty($map[$field_name]['bundles'])) {
        unset($map[$field_name]);
      }
    }
    $field_map_kv_store->set($entity_type, $map);
    $this->cleanUpEmptyEntityType($entity_type);
  }

  /**
   * Clean up empty entity types.
   */
  public function cleanUpEmptyEntityType($entity_type) {
    $field_map_kv_store = $this->keyValueStore->get('entity.definitions.bundle_field_map');
    // Additional cleanup suggested by vladdancer
    if (empty($field_map_kv_store->get($entity_type))) {
      $field_map_kv_store->delete($entity_type);
    }
  }

}
