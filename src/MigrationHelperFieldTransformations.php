<?php

namespace Drupal\migration_helpers;

use Drupal\Core\Entity\EntityTypeManagerInterface;

class MigrationHelperFieldTransformations {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs an MigrationHelperFieldTransformations object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Move data from file/image fields into media reference fields.
   *
   * Default transformation is image fields.
   */
  public function fieldToMediaEntity($entity_type, $source_field_names, $media_field_name, $source_entity_bundles = NULL, $media_entity_bundle = 'image', $media_target_field = 'field_media_image') {
    $media_entity_storage = $this->entityTypeManager->getStorage('media');
    $source_entity_storage = $this->entityTypeManager->getStorage($entity_type);
    $entity_query = $source_entity_storage->getQuery();

    $group = $entity_query->orConditionGroup();
    if (is_string($source_field_names)) {
      $source_field_names = [$source_field_names];
    }
    foreach ($source_field_names as $field) {
      $group->exists($field);
    }
    $entity_query->condition($group);

    if (!\is_null($source_entity_bundles)) {
      $bundle_key = $source_entity_storage->getEntityType()->getKey('bundle');
      $entity_query->condition($bundle_key, (array) $source_entity_bundles, 'IN');
    }

    $entity_query->accessCheck(FALSE);
    $results = $entity_query->execute();

    $entities = $source_entity_storage->loadMultiple($results);

    foreach ($entities as $entity) {
      $media_target_ids = [];
      $langcodes = $entity->getTranslationLanguages();
      $translations = [];
      if ($langcodes > 0) {
        foreach ($langcodes as $langcode) {
          $check_translation = $entity->getTranslation($langcode->getId());
          if ($check_translation !== NULL) {
            $translations[] = $entity->getTranslation($langcode->getId());
          }
        }
      }

      foreach ($translations as $translation) {

        foreach ($source_field_names as $source_field_name) {
          foreach ($translation->$source_field_name as $fieldItem) {
            $entity_data = [
              'bundle' => $media_entity_bundle,
              'langcode' => $fieldItem->getLangcode(),
              'status' => 1,
            ];
            // Some entities (like taxonomy terms) do not have a an owner id
            if (\method_exists($entity, 'getOwnerId')) {
              $entity_data['uid']  = $entity->getOwnerId();
            }

            foreach (\array_keys($fieldItem->getValue()) as $subfield) {
              $entity_data[$media_target_field][$subfield] = $fieldItem->get($subfield)->getValue();
            }

            // Check if we have a media entity for the same file already.
            $file_id = $entity_data[$media_target_field]['target_id'];
            $existing_media_query = $media_entity_storage->getQuery();
            $existing_media_query->condition($media_target_field, $file_id);
            $existing_media_query->accessCheck(FALSE);
            $existing_media_query_results = $existing_media_query->execute();
            if ($existing_media_query_results) {
              $media_id = array_pop($existing_media_query_results);
            }
            else {
              $media_entity = $media_entity_storage->create($entity_data);
              $media_entity->save();
              $media_id = $media_entity->id();
            }

            $media_target_ids[] = $media_id;
          }
        }
        $translation->set($media_field_name, $media_target_ids);
      }
      $entity->save();
    }

    $source_fields = implode(', ', $source_field_names);
    \Drupal::logger('nchfa_custom')->info("Data in field(s) $source_fields of $entity_type entity moved to $media_field_name media reference field.");
  }
}
